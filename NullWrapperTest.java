import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author ltoshchev
 * @created 04/04/14.
 */
public class NullWrapperTest {
    public static void main(String[] args) {
        System.out.println(NullWrapper.wrapper(foo(), HashSet.class));
        System.out.println(NullWrapper.wrapper(bar(), HashSet.class));
    }

    public static Set<Integer> foo() {
        Set<Integer> tmp = new HashSet<>();
        tmp.add(1);
        tmp.add(2);
        tmp.add(3);
        return tmp;
    }

    public static Set<Integer> bar() {
        return null;
    }
}
