/**
 * @author ltoshchev
 * @created 04/04/14.
 */
public class NullWrapper {
    public static <E> E wrapper(E obj, Class<? extends E> clazz) {
        if (obj != null) {
            return obj;
        }
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalArgumentException("Invalid class for dynamic instantiation.", e);
        }
    }

    public static <E> E wrapper(E obj, E defaultResult, Class<? extends E> clazz) {
        if (obj != null) {
            return obj;
        }
        if (defaultResult != null) {
            return defaultResult;
        }
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalArgumentException("Invalid class for dynamic instantiation.", e);
        }
    }
}
